<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:t="https://gilab.com/maccesch/bible-history"
                xmlns="http://www.w3.org/2000/svg">
    <xsl:variable name="timeScale" select="1 div 50"/>
    <xsl:variable name="marginTop" select="50"/>
    <xsl:variable name="marginBottom" select="20"/>
    <xsl:variable name="layerWidth" select="300"/>


    <xsl:variable name="startTimeNode" select="//t:period[not(t:start/@year &gt; ../t:period/t:start/@year)]/t:start"/>
    <xsl:variable name="endTimeNode" select="//t:period[not(t:end/@year &lt; ../t:period/t:end/@year)]/t:end"/>
    <xsl:variable name="documentHeight">
        <xsl:variable name="temp">
            <xsl:call-template name="calc-time-coord-val">
                <xsl:with-param name="year" select="(floor(($endTimeNode/@year+1) div 10) + 1) * 10"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$temp + $marginBottom"/>
    </xsl:variable>


    <xsl:template name="calc-time-coord-val">
        <xsl:param name="year"/>
        <xsl:param name="month" select="1"/>
        <xsl:param name="monthBias" select="0"/>
        <xsl:param name="displayBias" select="0"/>

        <xsl:variable name="noZeroCompensation">
            <xsl:choose>
                <xsl:when test="$year &lt; 0">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:value-of
                select="round((($year - $startTimeNode/@year + $noZeroCompensation) * 360 + ($month - $monthBias) * 30) * $timeScale) + $displayBias + $marginTop"/>
    </xsl:template>


    <xsl:template name="calc-time-coord">
        <xsl:param name="timeNode"/>

        <xsl:variable name="month">
            <xsl:choose>
                <xsl:when test="$timeNode/@month">
                    <xsl:value-of select="$timeNode/@month"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="name($timeNode) = 'end'">12</xsl:when>
                        <xsl:otherwise>1</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="monthBias">
            <xsl:choose>
                <xsl:when test="name($timeNode) = 'end'">0</xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="displayBias">
            <xsl:choose>
                <xsl:when test="$timeNode/@displayBias">
                    <xsl:value-of select="$timeNode/@displayBias"/>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:call-template name="calc-time-coord-val">
            <xsl:with-param name="year" select="$timeNode/@year"/>
            <xsl:with-param name="month" select="$month"/>
            <xsl:with-param name="monthBias" select="$monthBias"/>
            <xsl:with-param name="displayBias" select="$displayBias"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template name="calc-uncolliding-time-coord">
        <xsl:param name="timeNode"/>
        <xsl:param name="titleCoord"/>
        <xsl:param name="checkTitle" select="true()"/>

        <xsl:variable name="initCoord">
            <xsl:call-template name="calc-time-coord">
                <xsl:with-param name="timeNode" select="$timeNode"/>
            </xsl:call-template>
        </xsl:variable>

        <!-- start/end title -->
        <xsl:variable name="limitBias">
            <xsl:choose>
                <xsl:when test="($initCoord - $titleCoord) * ($initCoord - $titleCoord) &lt; 5*5">3</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="name($timeNode) = 'start'">6</xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="name($timeNode) = 'end'">-6</xsl:when>
                                <xsl:otherwise>0</xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- check collision with period title -->
        <xsl:variable name="titleBias">
            <xsl:choose>
                <xsl:when
                        test="$checkTitle and ($initCoord + $limitBias - $titleCoord) * ($initCoord + $limitBias - $titleCoord) &lt; 13*13">
                    <xsl:value-of select="13 - ($initCoord + $limitBias - $titleCoord)"/>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- check collision with previous event -->
        <xsl:variable name="prevCoord">
            <xsl:choose>
                <!-- previous subevent -->
                <xsl:when test="$titleBias = 0 and $timeNode/../preceding-sibling::t:event[1]">
                    <xsl:call-template name="calc-uncolliding-time-coord">
                        <xsl:with-param name="timeNode" select="$timeNode/../preceding-sibling::t:event[1]/t:time"/>
                        <xsl:with-param name="titleCoord" select="$titleCoord"/>
                        <xsl:with-param name="checkTitle" select="$checkTitle"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <!-- previous start event -->
                        <xsl:when
                                test="$titleBias = 0 and $timeNode/../../preceding-sibling::t:start/t:title | $timeNode/preceding-sibling::t:start">
                            <xsl:call-template name="calc-uncolliding-time-coord">
                                <xsl:with-param name="timeNode"
                                                select="$timeNode/../../preceding-sibling::t:start | $timeNode/preceding-sibling::t:start"/>
                                <xsl:with-param name="titleCoord" select="$titleCoord"/>
                                <xsl:with-param name="checkTitle" select="$checkTitle"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="eventBias">
            <xsl:choose>
                <xsl:when
                        test="($initCoord + $limitBias - $prevCoord) * ($initCoord + $limitBias - $prevCoord) &lt; 13*13">
                    <xsl:value-of select="13 - ($initCoord + $limitBias - $prevCoord)"/>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- finally calculate biased value -->
        <xsl:value-of select="$initCoord + $titleBias + $limitBias + $eventBias"/>
    </xsl:template>


    <xsl:template name="calc-layer-coord">
        <xsl:param name="category"/>

        <xsl:value-of select="(//t:categories/t:category[@id=$category]/@displayLayer - 1) * $layerWidth + 160"/>
    </xsl:template>


    <xsl:template name="format-year-val">
        <xsl:param name="year"/>
        <xsl:param name="sure" select="'true'"/>
        <xsl:param name="exact" select="'true'"/>
        <xsl:param name="secular" select="'false'"/>

        <xsl:if test="$secular = 'true'">*</xsl:if>

        <xsl:if test="$sure = 'false'">?</xsl:if>

        <xsl:if test="$exact = 'false'">ca.</xsl:if>

        <xsl:choose>
            <xsl:when test="$year &lt; 0">
                <xsl:value-of select="-$year"/> v.u.Z.
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$year"/> u.Z.
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template name="format-year">
        <xsl:param name="timeNode"/>

        <xsl:call-template name="format-year-val">
            <xsl:with-param name="year" select="$timeNode/@year"/>
            <xsl:with-param name="sure" select="$timeNode/@sure"/>
            <xsl:with-param name="exact" select="$timeNode/@exact"/>
            <xsl:with-param name="secular" select="$timeNode/@secular"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template name="subevent">
        <xsl:param name="x"/>
        <xsl:param name="y"/>
        <xsl:param name="textY"/>
        <xsl:param name="drawLine" select="true()"/>
        <xsl:param name="text"/>
        <xsl:param name="sources"/>
        <xsl:param name="time"/>

        <xsl:variable name="textNocollCoord">
            <xsl:call-template name="calc-uncolliding-time-coord">
                <xsl:with-param name="timeNode" select="$time"/>
                <xsl:with-param name="titleCoord" select="$textY"/>
            </xsl:call-template>
        </xsl:variable>

        <g class="subevent">
            <!-- 			<rect class="dummy" width="21" height="16" opacity="0">
                            <xsl:attribute name="x"><xsl:value-of select="$x - 8" /></xsl:attribute>
                            <xsl:attribute name="y"><xsl:value-of select="$y - 8" /></xsl:attribute>
                        </rect>  -->
            <xsl:if test="$drawLine and (not($time/@fuzzy) or $time/@fuzzy != 'true')">
                <text class="year">
                    <xsl:attribute name="x">
                        <xsl:value-of select="$x - 5"/>
                    </xsl:attribute>
                    <xsl:attribute name="y">
                        <xsl:value-of select="$textNocollCoord"/>
                    </xsl:attribute>
                    <xsl:call-template name="format-year">
                        <xsl:with-param name="timeNode" select="$time"/>
                    </xsl:call-template>
                </text>
                <line>
                    <xsl:attribute name="x1">
                        <xsl:value-of select="$x"/>
                    </xsl:attribute>
                    <xsl:attribute name="x2">
                        <xsl:value-of select="$x + 5"/>
                    </xsl:attribute>
                    <xsl:attribute name="y1">
                        <xsl:value-of select="$y"/>
                    </xsl:attribute>
                    <xsl:attribute name="y2">
                        <xsl:value-of select="$y"/>
                    </xsl:attribute>
                </line>
            </xsl:if>
            <text class="title">
                <xsl:attribute name="x">
                    <xsl:value-of select="$x + 10"/>
                </xsl:attribute>
                <xsl:attribute name="y">
                    <xsl:value-of select="$textNocollCoord"/>
                </xsl:attribute>
                <xsl:value-of select="$text"/>
            </text>
            <xsl:for-each select="$sources">
                <text class="sources">
                    <xsl:attribute name="x">
                        <xsl:value-of select="$x + 10"/>
                    </xsl:attribute>
                    <xsl:attribute name="y">
                        <xsl:value-of select="$y + 12 * position()"/>
                    </xsl:attribute>
                    <xsl:value-of select="text()"/>
                </text>
            </xsl:for-each>
        </g>
    </xsl:template>


    <xsl:template name="time-lines">
        <xsl:param name="startYear"/>
        <xsl:param name="endYear"/>

        <xsl:variable name="y">
            <xsl:call-template name="calc-time-coord-val">
                <xsl:with-param name="year" select="$startYear"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="mark">
            <xsl:if test="$startYear mod 50 = 0">i</xsl:if>
            <xsl:if test="$startYear mod 100 = 0">i</xsl:if>
            <xsl:if test="$startYear mod 1000 = 0">i</xsl:if>
        </xsl:variable>

        <g class="time-line">
            <!-- line across the whole doc -->
            <line x1="70px" x2="100%">
                <xsl:attribute name="y1">
                    <xsl:value-of select="$y"/>
                </xsl:attribute>
                <xsl:attribute name="y2">
                    <xsl:value-of select="$y"/>
                </xsl:attribute>

                <xsl:attribute name="class">time-line
                    <xsl:value-of select="$mark"/>
                </xsl:attribute>
            </line>

            <!-- mark at the scale -->
            <line class="time-mark" x2="70px">
                <xsl:attribute name="x1">
                    <xsl:value-of select="67 - string-length($mark)*2"/>
                </xsl:attribute>
                <xsl:attribute name="y1">
                    <xsl:value-of select="$y"/>
                </xsl:attribute>
                <xsl:attribute name="y2">
                    <xsl:value-of select="$y"/>
                </xsl:attribute>
            </line>

            <!-- year texts -->
            <text class="scale-text" x="60">
                <xsl:attribute name="y">
                    <xsl:value-of select="$y"/>
                </xsl:attribute>

                <xsl:call-template name="format-year-val">
                    <xsl:with-param name="year" select="$startYear"/>
                </xsl:call-template>
            </text>
        </g>

        <xsl:if test="$startYear + 10 &lt;= $endYear">
            <xsl:call-template name="time-lines">
                <xsl:with-param name="startYear" select="$startYear + 10"/>
                <xsl:with-param name="endYear" select="$endYear"/>
            </xsl:call-template>
        </xsl:if>

    </xsl:template>


    <xsl:template name="time-scale">
        <xsl:param name="startYear"/>
        <xsl:param name="endYear"/>

        <g class="time-scale">
            <line class="time-scale" x1="70" x2="70">
                <xsl:attribute name="y1">
                    <xsl:call-template name="calc-time-coord-val">
                        <xsl:with-param name="year" select="$startYear"/>
                    </xsl:call-template>
                </xsl:attribute>

                <xsl:attribute name="y2">
                    <xsl:call-template name="calc-time-coord-val">
                        <xsl:with-param name="year" select="$endYear"/>
                    </xsl:call-template>
                </xsl:attribute>
            </line>

            <xsl:call-template name="time-lines">
                <xsl:with-param name="startYear" select="$startYear"/>
                <xsl:with-param name="endYear" select="$endYear"/>
            </xsl:call-template>
        </g>

    </xsl:template>


    <xsl:template name="draw-era">
        <xsl:param name="category"/>

        <xsl:variable name="catName" select="$category/@id"/>

        <xsl:variable name="x">
            <xsl:call-template name="calc-layer-coord">
                <xsl:with-param name="category" select="$category/@id"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="endY">
            <xsl:call-template name="calc-time-coord">
                <xsl:with-param name="timeNode"
                                select="//t:period[normalize-space(t:category/text()) = $catName and not(t:end/@year &lt; ../t:period[normalize-space(t:category/text()) = $catName]/t:end/@year)]/t:end"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="startY">
            <xsl:call-template name="calc-time-coord">
                <xsl:with-param name="timeNode"
                                select="//t:period[normalize-space(t:category/text()) = $catName and not(t:start/@year &gt; ../t:period[normalize-space(t:category/text()) = $catName]/t:start/@year)]/t:start"/>
            </xsl:call-template>
        </xsl:variable>

        <rect>
            <xsl:attribute name="class">
                <xsl:text>era </xsl:text>
                <xsl:value-of select="$catName"/>
            </xsl:attribute>

            <xsl:attribute name="x">
                <xsl:value-of select="$x - 75"/>
            </xsl:attribute>
            <xsl:attribute name="y">
                <xsl:value-of select="$startY"/>
            </xsl:attribute>
            <xsl:attribute name="height">
                <xsl:value-of select="$endY - $startY"/>
            </xsl:attribute>
            <xsl:attribute name="width">
                <xsl:value-of select="$layerWidth - 1"/>
            </xsl:attribute>
        </rect>

        <text class="era-title" x="0" y="0" transform="translate(300, 100) rotate(-90)">
            <xsl:attribute name="transform">
                <xsl:text>translate(</xsl:text>
                <xsl:value-of select="$x + $layerWidth - 75 - 7"/>
                <xsl:text>,</xsl:text>
                <xsl:value-of select="$startY + 7"/>
                <xsl:text>) rotate(-90)</xsl:text>
            </xsl:attribute>

            <xsl:value-of select="normalize-space(t:title/text())"/>
        </text>
    </xsl:template>


    <xsl:template match="/">

        <svg>
            <xsl:attribute name="height"><xsl:value-of select="$documentHeight"/></xsl:attribute>
            <defs>
                <linearGradient id="gradient-red-green" x1="0" y1="0" x2="0" y2="1">
                    <stop stop-color="red" offset="0.1"/>
                    <stop stop-color="green" offset="0.9"/>
                </linearGradient>
                <linearGradient id="gradient-green-red" x1="0" y1="0" x2="0" y2="1">
                    <stop stop-color="green" offset="0.1"/>
                    <stop stop-color="red" offset="0.9"/>
                </linearGradient>
                <linearGradient id="gradient-wb" x1="0" y1="0" x2="0" y2="1">
                    <stop stop-color="white" offset="0.8"/>
                    <stop stop-color="black" offset="0.95"/>
                </linearGradient>
                <linearGradient id="gradient-bw" x1="0" y1="0" x2="0" y2="1">
                    <stop stop-color="black" offset="0.05"/>
                    <stop stop-color="white" offset="0.2"/>
                </linearGradient>
                <linearGradient id="gradient-bwb" x1="0" y1="0" x2="0" y2="1">
                    <stop stop-color="black" offset="0.05"/>
                    <stop stop-color="white" offset="0.2"/>
                    <stop stop-color="white" offset="0.8"/>
                    <stop stop-color="black" offset="0.95"/>
                </linearGradient>
            </defs>
            <style type="text/css">
                svg {
                    font-family: sans-serif;
                    font-size: 12px;
                }

                text {
                    dominant-baseline: central;
                }

                path.period-path {
                    stroke-width: 2px;
                    stroke-linecap: butt;
                    stroke-linejoin: miter;
                    stroke-opacity: 1;
                    fill: none;
                }

                text.period-title {
                    font-weight: bold;
                }

                g.special path.period-path {
                    stroke-dasharray: 10, 4;
                }

                g.special text.period-title {
                    font-style: italic;
                }

                text.period-start, text.period-end {
                    font-size: 11px;
                    text-anchor: end;
                }

                g.subevent text {
                    font-size: 12px;
                }

                g.subevent text.year {
                    fill: grey;
                    font-size: 11px;
                    text-anchor: end;
                }

                g.subevent text.sources {
                    display: none;
                }

                g.subevent line {
                    stroke-width: 2px;
                    stroke: grey;
                }

                g.subevent:hover line {
                    stroke-width: 2px;
                    stroke: black;
                }

                line.time-scale, line.time-mark {
                    stroke: grey;
                    stroke-width: 2px;
                }

                line.time-line {
                    stroke: grey;
                    stroke-width: 2px;
                    opacity: 0.1;
                }

                line.time-line.i {
                   opacity: 0.2;
                }

                line.time-line.ii {
                    opacity: 0.3;
                }

                line.time-line.iii {
                   opacity: 0.4;
                }

                text.scale-text {
                    text-anchor: end;
                    font-size: 10px;
                }

                text.era-title {
                    text-anchor: end;
                    font-size: 16px;
                    font-weight: bold;
                    dominant-baseline: auto;
                    opacity: 0.3;
                }

                rect.era {
                    stroke: none;
                    opacity: 0.1;
                }

                rect.era.Kings.Twelve {
                    fill: #55ff00;
                }

                rect.era.Kings.Jerusalem {
                    fill: #ffff00;
                }

                rect.era.Governors.Jerusalem {
                    fill: #88ff00;
                }

                rect.era.Kings.Israel {
                   fill: #ff8800;
                }
            </style>
            <title>Zeittafel der Bibel</title>

            <xsl:call-template name="time-scale">
                <xsl:with-param name="startYear" select="floor($startTimeNode/@year div 10) * 10"/>
                <xsl:with-param name="endYear" select="(floor(($endTimeNode/@year+1) div 10) + 1) * 10"/>
            </xsl:call-template>

            <xsl:for-each select="/t:history/t:categories/t:category[(not(@display) or @display != 'false')]">
                <xsl:variable name="category" select="@id"/>

                <g class="category">
                    <xsl:attribute name="id">
                        <xsl:value-of select="$category"/>
                    </xsl:attribute>

                    <xsl:if test="t:title">
                        <xsl:call-template name="draw-era">
                            <xsl:with-param name="category" select="."/>
                        </xsl:call-template>
                    </xsl:if>

                    <xsl:for-each select="/t:history/t:period[normalize-space(t:category/text()) = $category]">
                        <xsl:sort select="t:start/@year"/>
                        <xsl:variable name="startCoord">
                            <xsl:call-template name="calc-time-coord">
                                <xsl:with-param name="timeNode" select="t:start"/>
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:variable name="endCoord">
                            <xsl:call-template name="calc-time-coord">
                                <xsl:with-param name="timeNode" select="t:end"/>
                            </xsl:call-template>
                        </xsl:variable>

                        <xsl:variable name="limitBias">
                            <xsl:choose>
                                <xsl:when test="$endCoord &lt;= $startCoord + 4">0</xsl:when>
                                <xsl:otherwise>2</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>

                        <xsl:variable name="strokeColor">
                            <xsl:if test="normalize-space(t:start/text())='good' and normalize-space(t:end/text())='bad'">
                                url(#gradient-green-red)
                            </xsl:if>
                            <xsl:if test="normalize-space(t:start/text())='bad' and normalize-space(t:end/text())='good'">
                                url(#gradient-red-green)
                            </xsl:if>
                            <xsl:if test="normalize-space(t:start/text())='bad' and normalize-space(t:end/text())!='good'">
                                red
                            </xsl:if>
                            <xsl:if test="normalize-space(t:start/text())='good' and normalize-space(t:end/text())!='bad'">
                                green
                            </xsl:if>
                            <xsl:if test="normalize-space(t:start/text())!='good' and normalize-space(t:start/text())!='bad' and normalize-space(t:end/text())!='good' and normalize-space(t:end/text())!='bad'">
                                violet
                            </xsl:if>
                        </xsl:variable>

                        <xsl:variable name="layerCoord">
                            <xsl:call-template name="calc-layer-coord">
                                <xsl:with-param name="category" select="$category"/>
                            </xsl:call-template>
                        </xsl:variable>

                        <xsl:variable name="currentPeriod" select="."/>

                        <xsl:variable name="overlapBias">
                            <xsl:variable name="temp2">
                                <xsl:for-each
                                        select="//t:period[. != $currentPeriod and t:end/@year &gt; $currentPeriod/t:start/@year and t:end/@year &lt; $currentPeriod/t:end/@year]">
                                    <xsl:variable name="prevLayerCoord">
                                        <xsl:call-template name="calc-layer-coord">
                                            <xsl:with-param name="category" select="normalize-space(t:category/text())"/>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:if test="$layerCoord = $prevLayerCoord">true</xsl:if>
                                </xsl:for-each>
                            </xsl:variable>

                            <xsl:choose>
                                <xsl:when test="contains($temp2, 'true')">3</xsl:when>
                                <xsl:otherwise>0</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>

                        <xsl:variable name="textCoord" select="($startCoord + $endCoord) div 2"/>

                        <g>
                            <xsl:attribute name="class">
                                <xsl:value-of select="normalize-space(t:category/text())"/>
                                <xsl:if test="@special = 'true'">
                                    <xsl:text> special</xsl:text>
                                </xsl:if>
                            </xsl:attribute>

                            <!-- Period title -->
                            <text class="period-title">
                                <xsl:attribute name="y">
                                    <xsl:value-of select="$textCoord"/>
                                </xsl:attribute>
                                <xsl:attribute name="x">
                                    <xsl:value-of select="$layerCoord + 10"/>
                                </xsl:attribute>
                                <xsl:value-of select="t:title/text()"/>
                            </text>

                            <xsl:variable name="maskId">
                                <xsl:text>mask-fuzzy-</xsl:text>
                                <xsl:value-of select="translate($category, ' ', '-')"/>-<xsl:value-of
                                    select="position()"/>
                            </xsl:variable>

                            <xsl:if test="t:end/@fuzzy = 'true' or t:start/@fuzzy = 'true'">
                                <mask>
                                    <xsl:attribute name="id">
                                        <xsl:value-of select="$maskId"/>
                                    </xsl:attribute>
                                    <rect width="200" stroke="none">
                                        <xsl:attribute name="fill">
                                            <xsl:if test="not(t:start/@fuzzy) or t:start/@fuzzy != 'true'">
                                                url(#gradient-wb)
                                            </xsl:if>
                                            <xsl:if test="not(t:end/@fuzzy) or t:end/@fuzzy != 'true'">url(#gradient-bw)
                                            </xsl:if>
                                            <xsl:if test="t:end/@fuzzy = 'true' and t:start/@fuzzy = 'true'">
                                                url(#gradient-bwb)
                                            </xsl:if>
                                        </xsl:attribute>
                                        <xsl:attribute name="x">
                                            <xsl:value-of select="$layerCoord - 50"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="y">
                                            <xsl:value-of select="$startCoord"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="height">
                                            <xsl:value-of select="$endCoord - $startCoord"/>
                                        </xsl:attribute>
                                    </rect>
                                </mask>
                            </xsl:if>

                            <!-- Period span path -->
                            <xsl:if test="$endCoord &gt; $startCoord + 4">
                                <path class="period-path">
                                    <xsl:attribute name="d">m
                                        <xsl:value-of select="$layerCoord + 7"/>,<xsl:value-of
                                                select='$startCoord + $limitBias'/>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="-$overlapBias - 7"/>,0
                                        0,<xsl:value-of select='$endCoord - $limitBias*2 - $startCoord'/>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="$overlapBias + 7"/>,0
                                    </xsl:attribute>

                                    <xsl:attribute name="stroke">
                                        <xsl:value-of select='$strokeColor'/>
                                    </xsl:attribute>

                                    <xsl:if test="t:end/@fuzzy = 'true' or t:start/@fuzzy = 'true'">
                                        <xsl:attribute name="mask">url(#<xsl:value-of select="$maskId"/>)
                                        </xsl:attribute>
                                    </xsl:if>
                                </path>
                            </xsl:if>

                            <!-- start year -->
                            <xsl:if test="t:start/@displayYear = 'true' or ((not(t:start/@fuzzy) or (t:start/@fuzzy != 'true')) and $endCoord &gt; $startCoord + 4)">
                                <text class="period-start">
                                    <xsl:attribute name="x">
                                        <xsl:value-of select="$layerCoord - 5"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="y">
                                        <xsl:call-template name="calc-uncolliding-time-coord">
                                            <xsl:with-param name="timeNode" select="t:start"/>
                                            <xsl:with-param name="titleCoord" select="$textCoord"/>
                                            <xsl:with-param name="checkTitle" select="false()"/>
                                        </xsl:call-template>
                                    </xsl:attribute>
                                    <xsl:call-template name="format-year">
                                        <xsl:with-param name="timeNode" select="t:start"/>
                                    </xsl:call-template>
                                </text>
                            </xsl:if>

                            <!-- end year -->
                            <xsl:if test="t:end/@displayYear = 'true'">
                                <text class="period-end">
                                    <xsl:attribute name="x">
                                        <xsl:value-of select="$layerCoord - 5"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="y">
                                        <xsl:call-template name="calc-uncolliding-time-coord">
                                            <xsl:with-param name="timeNode" select="t:end"/>
                                            <xsl:with-param name="titleCoord" select="$textCoord"/>
                                            <xsl:with-param name="checkTitle" select="false()"/>
                                        </xsl:call-template>
                                    </xsl:attribute>
                                    <xsl:call-template name="format-year">
                                        <xsl:with-param name="timeNode" select="t:end"/>
                                    </xsl:call-template>
                                </text>
                            </xsl:if>

                            <!-- Start event -->
                            <xsl:if test="t:start/t:title">
                                <xsl:call-template name="subevent">
                                    <xsl:with-param name="x" select="$layerCoord"/>
                                    <xsl:with-param name="y" select="$startCoord"/>
                                    <xsl:with-param name="textY" select="$textCoord"/>
                                    <xsl:with-param name="text" select="t:start/t:title/text()"/>
                                    <xsl:with-param name="time" select="t:start"/>
                                    <xsl:with-param name="drawLine" select="false()"/>
                                    <xsl:with-param name="sources" select="t:start/t:sources"/>
                                </xsl:call-template>
                            </xsl:if>

                            <!-- End event -->
                            <xsl:if test="t:end/t:title">
                                <xsl:call-template name="subevent">
                                    <xsl:with-param name="x" select="$layerCoord"/>
                                    <xsl:with-param name="y" select="$endCoord - 3"/>
                                    <xsl:with-param name="textY" select="$textCoord"/>
                                    <xsl:with-param name="time" select="t:end"/>
                                    <xsl:with-param name="text" select="t:end/t:title/text()"/>
                                    <xsl:with-param name="drawLine" select="false()"/>
                                    <xsl:with-param name="sources" select="t:end/t:sources"/>
                                </xsl:call-template>
                            </xsl:if>

                            <!-- Subhistory -->
                            <xsl:for-each select="t:subhistory/t:event">
                                <xsl:variable name="subCoord">
                                    <xsl:call-template name="calc-time-coord">
                                        <xsl:with-param name="timeNode" select="t:time"/>
                                    </xsl:call-template>
                                </xsl:variable>

                                <xsl:call-template name="subevent">
                                    <xsl:with-param name="x" select="$layerCoord"/>
                                    <xsl:with-param name="y" select="$subCoord"/>
                                    <xsl:with-param name="textY" select="$textCoord"/>
                                    <xsl:with-param name="time" select="t:time"/>
                                    <xsl:with-param name="text" select="t:title/text()"/>
                                    <xsl:with-param name="sources" select="t:sources"/>
                                </xsl:call-template>
                            </xsl:for-each>
                        </g>
                    </xsl:for-each>
                </g>
            </xsl:for-each>
        </svg>
    </xsl:template>
</xsl:stylesheet>